package com.thrift.redis.commands;

import java.io.Closeable;



import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Pattern;

import org.apache.thrift.TException;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.JedisShardInfo;
import redis.clients.jedis.ShardedJedis;
import redis.clients.jedis.ShardedJedisPool;
import redis.clients.util.Hashing;
import redis.clients.util.Sharded;

import com.alibaba.fastjson.JSON;
import com.thrift.DateUtil;


public class RedisCommandsImpl  extends Sharded<Jedis, JedisShardInfo> implements RedisCommands.Iface, RedisCommands1.Iface,Closeable{

	public static ShardedJedisPool pools = null;
	public static JedisPoolConfig config = null;
	private static ShardedJedis sj = null;
	private static Properties prop = null;
	
	public RedisCommandsImpl(List<JedisShardInfo> shards) {
		super(shards);
		
		prop = new Properties();
		InputStream is = null;
	    try {
			is = new FileInputStream("config/config.properties");
			prop.load(is);
			config = new JedisPoolConfig();
			//config.setMaxActive(Integer.parseInt(prop.getProperty("maxActive")));
			config.setMaxIdle(Integer.parseInt(prop.getProperty("maxIdle")));
			config.setMaxWaitMillis(Integer.parseInt(prop.getProperty("maxWait")));
			config.setMinIdle(Integer.parseInt(prop.getProperty("minIdle")));
			config.setTestOnBorrow(Boolean.parseBoolean(prop.getProperty("testOnBorrow")));
			config.setTestOnReturn(Boolean.parseBoolean(prop.getProperty("testOnReturn")));
			
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		pools = new ShardedJedisPool(config,shards);
		
	}
	
	public RedisCommandsImpl(List<JedisShardInfo> shards,Hashing algo){
		super(shards,algo);
		prop = new Properties();
		InputStream is = null;
	    try {
			is = new FileInputStream("config/config.properties");
			prop.load(is);
			config = new JedisPoolConfig();
			//config.setMaxActive(Integer.parseInt(prop.getProperty("maxActive")));
			config.setMaxIdle(Integer.parseInt(prop.getProperty("maxIdle")));
			config.setMaxWaitMillis(Integer.parseInt(prop.getProperty("maxWait")));
			config.setMinIdle(Integer.parseInt(prop.getProperty("minIdle")));
			config.setTestOnBorrow(Boolean.parseBoolean(prop.getProperty("testOnBorrow")));
			config.setTestOnReturn(Boolean.parseBoolean(prop.getProperty("testOnReturn")));
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		pools = new ShardedJedisPool(config,shards);
	}
	
	public RedisCommandsImpl(List<JedisShardInfo> shards, Pattern keyTagPattern) {
		super(shards,keyTagPattern);
		prop = new Properties();
		InputStream is = null;
	    try {
			is = new FileInputStream("config/config.properties");
			prop.load(is);
			config = new JedisPoolConfig();
			
			//config.setMaxActive(Integer.parseInt(prop.getProperty("maxActive")));
			config.setMaxIdle(Integer.parseInt(prop.getProperty("maxIdle")));
			config.setMaxWaitMillis(Integer.parseInt(prop.getProperty("maxWait")));
			config.setMinIdle(Integer.parseInt(prop.getProperty("minIdle")));
			config.setTestOnBorrow(Boolean.parseBoolean(prop.getProperty("testOnBorrow")));
			config.setTestOnReturn(Boolean.parseBoolean(prop.getProperty("testOnReturn")));
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		pools = new ShardedJedisPool(config,shards);
	}
	
	
	public void disconnect() throws IOException {
		for (Jedis jedis : getAllShards()) {
			jedis.disconnect();
		}
	}
	
	protected Jedis create(JedisShardInfo shard) {
		return new Jedis(shard);
	}

	@Override
	public String get(String key){
		sj = pools.getResource();
		String value = "";
		try{
			value = sj.get(DateUtil.format(new Date())+"_"+key);
		}catch(Exception e){
			pools.returnBrokenResource(sj);
			return "";
		}
		pools.returnResource(sj);
		return value;
	}

	@Override
	public void close(){
		
	}

	@Override
	public boolean exists(String key){
		sj = pools.getResource();
		boolean value = false;
		try{
			value = sj.exists(key);
		}catch(Exception e){
			pools.returnBrokenResource(sj);	
		}
		pools.returnResource(sj);
		return value;
		
	}

	@Override
	public long persist(String key) throws TException {
		sj = pools.getResource();
		Jedis j = sj.getShard(key);
		long value = j.persist(key);
		pools.returnResource(sj);
		return value;
	}

	@Override
	public String type(String key) throws TException {
		sj = pools.getResource();
		String value = sj.type(key);
		pools.returnResource(sj);
		return value;
	}

	@Override
	public long expire(String key, int seconds) throws TException {
		sj = pools.getResource();
		long value = sj.expire(key, seconds);
		pools.returnResource(sj);
		return value;
	}

	@Override
	public long expireAt(String key, long unixTime) throws TException {
		sj = pools.getResource();
		long value = sj.expireAt(key, unixTime);
		pools.returnResource(sj);
		return value;
	}

	@Override
	public long ttl(String key) throws TException {
		sj = pools.getResource();
		long value = sj.ttl(key);
		pools.returnResource(sj);
		return value;
	}

	@Override
	public boolean getbit(String key, long offset) throws TException {
		sj = pools.getResource();
		boolean value = sj.getbit(key, offset);
		pools.returnResource(sj);
		return value;
	}

	@Override
	public String getrange(String key, long startOffset, long endOffset)
			throws TException {
		sj = pools.getResource();
		String value = sj.getrange(key, startOffset,endOffset);
		pools.returnResource(sj);
		return value;
	}

	@Override
	public String getSet(String key, String values) throws TException {
		sj = pools.getResource();
		String value = sj.getSet(key, values);
		pools.returnResource(sj);
		return value;
	}

	@Override
	public long decrBy(String key, long integer) throws TException {
		sj = pools.getResource();
		long value = sj.decrBy(key, integer);
		pools.returnResource(sj);
		return value;
	}

	@Override
	public long decr(String key) throws TException {
		sj = pools.getResource();
		long value = sj.decr(key);
		pools.returnResource(sj);
		return value;
	}

	@Override
	public long incrBy(String key, long integer) throws TException {
		sj = pools.getResource();
		long value = sj.incrBy(key,integer);
		pools.returnResource(sj);
		return value;
	}

	@Override
	public long append(String key, String values) throws TException {
		sj = pools.getResource();
		long value = sj.append(key,values);
		pools.returnResource(sj);
		return value;
	}

	@Override
	public String substr(String key, int start, int ends) throws TException {
		sj = pools.getResource();
		String value = sj.substr(key,start,ends);
		pools.returnResource(sj);
		return value;
	}

	@Override
	public String hget(String key, String field) throws TException {
		sj = pools.getResource();
		String value = sj.hget(key,field);
		pools.returnResource(sj);
		return value;
	}

	@Override
	public long hincrBy(String key, String field, long values) throws TException {
		sj = pools.getResource();
		long value = sj.hincrBy(key,field,values);
		pools.returnResource(sj);
		return value;
	}

	@Override
	public boolean hexists(String key, String field) throws TException {
		sj = pools.getResource();
		boolean value = sj.hexists(key,field);
		pools.returnResource(sj);
		return value;
	}

	@Override
	public long hlen(String key) throws TException {
		sj = pools.getResource();
		long value = sj.hlen(key);
		pools.returnResource(sj);
		return value;
	}

	@Override
	public String hkeys(String key) throws TException {
		
		sj = pools.getResource();
		String value = JSON.toJSONString(sj.hkeys(key));
		pools.returnResource(sj);
		return value;
		
		
	}

	@Override
	public String hvals(String key) throws TException {
		sj = pools.getResource();
		String value = JSON.toJSONString(sj.hvals(key));
		pools.returnResource(sj);
		return value;
	}

	@Override
	public String hgetAll(String key) throws TException {
		sj = pools.getResource();
		String value = JSON.toJSONString(sj.hgetAll(key));
		pools.returnResource(sj);
		return value;
	}

	@Override
	public String lrange(String key, long start, long ends) throws TException {
		sj = pools.getResource();
		String value = JSON.toJSONString(sj.lrange(key,start,ends));
		pools.returnResource(sj);
		return value;
	}

	@Override
	public String smembers(String key) throws TException {
		sj = pools.getResource();
		String value = JSON.toJSONString(sj.smembers(key));
		pools.returnResource(sj);
		return value;
	}

	@Override
	public long llen(String key) throws TException {
		sj = pools.getResource();
		long value = sj.llen(key);
		pools.returnResource(sj);
		return value;
	}

	@Override
	public String ltrim(String key, long start, long ends) throws TException {
		sj = pools.getResource();
		String value = JSON.toJSONString(sj.ltrim(key,start,ends));
		pools.returnResource(sj);
		return value;
	}

	@Override
	public String lindex(String key, long index) throws TException {
		sj = pools.getResource();
		String value = sj.lindex(key,index);
		pools.returnResource(sj);
		return value;
	}

	@Override
	public String lpop(String key) throws TException {
		sj = pools.getResource();
		String value = sj.lpop(key);
		pools.returnResource(sj);
		return value;
	}

	@Override
	public String rpop(String key) throws TException {
		sj = pools.getResource();
		String value = sj.rpop(key);
		pools.returnResource(sj);
		return value;
	}

	@Override
	public String spop(String key) throws TException {
		sj = pools.getResource();
		String value = sj.spop(key);
		pools.returnResource(sj);
		return value;
	}

	@Override
	public long scard(String key) throws TException {
		sj = pools.getResource();
		long value = sj.scard(key);
		pools.returnResource(sj);
		return value;
	}

	@Override
	public boolean sismember(String key, String member) throws TException {
		sj = pools.getResource();
		boolean value = sj.sismember(key,member);
		pools.returnResource(sj);
		return value;
	}

	@Override
	public String srandmember(String key) throws TException {
		sj = pools.getResource();
		String value = sj.srandmember(key);
		pools.returnResource(sj);
		return value;
	}

	@Override
	public long strlen(String key) throws TException {
		sj = pools.getResource();
		Jedis j = sj.getShard(key);
		long value = j.strlen(key);
		pools.returnResource(sj);
		return value;
	}

	@Override
	public double zincrby(String key, double score, String member)
			throws TException {
		sj = pools.getResource();
		double value = sj.zincrby(key,score,member);
		pools.returnResource(sj);
		return value;
	}

	@Override
	public long zrank(String key, String member) throws TException {
		sj = pools.getResource();
		long value = sj.zrank(key,member);
		pools.returnResource(sj);
		return value;
	}

	@Override
	public long zrevrand(String key, String member) throws TException {
		sj = pools.getResource();
		long value = sj.zrevrank(key,member);
		pools.returnResource(sj);
		return value;
	}

	@Override
	public String zrevrange(String key, long start, long ends)
			throws TException {
		sj = pools.getResource();
		String value = JSON.toJSONString(sj.zrevrange(key,start,ends));
		pools.returnResource(sj);
		return value;
		
	}

	@Override
	public long zcard(String key) throws TException {
		sj = pools.getResource();
		long value = sj.zcard(key);
		pools.returnResource(sj);
		return value;
	}

	@Override
	public double zscore(String key, String member) throws TException {
		sj = pools.getResource();
		double value = sj.zscore(key,member);
		pools.returnResource(sj);
		return value;
	}

	@Override
	public String sort(String key) throws TException {
		sj = pools.getResource();
		String value = JSON.toJSONString(sj.sort(key));
		pools.returnResource(sj);
		return value;
	}

	@Override
	public long zcount(String key, String min, String max) throws TException {
		sj = pools.getResource();
		long value = sj.zcount(key,min,max);
		pools.returnResource(sj);
		return value;
	}


	@Override
	public long zremrangeByRank(String key, long start, long ends)
			throws TException {
		sj = pools.getResource();
		long value = sj.zremrangeByRank(key,start,ends);
		pools.returnResource(sj);
		return value;
	}

	@Override
	public long zremrangeByScore(String key, String start, String ends)
			throws TException {
		sj = pools.getResource();
		long value = sj.zremrangeByScore(key,start,ends);
		pools.returnResource(sj);
		return value;
	}

	@Override
	public String blpop(int timeout, String key) throws TException {
		sj = pools.getResource();
		Jedis j = sj.getShard(key);
		String value = JSON.toJSONString(j.blpop(timeout,key));
		pools.returnResource(sj);
		return value;
	}

	@Override
	public String brpop(int timeout, String key) throws TException {
		sj = pools.getResource();
		Jedis j = sj.getShard(key);
		String value = JSON.toJSONString(j.brpop(timeout,key));
		pools.returnResource(sj);
		return value;
	}

	@Override
	public String echo(String str) throws TException {
		sj = pools.getResource();
		Jedis j = sj.getShard(str);
		String value = j.echo(str);
		pools.returnResource(sj);
		return value;
	}

	@Override
	public long move(String key, int dbIndex) throws TException {
		sj = pools.getResource();
		Jedis j = sj.getShard(key);
		long value = j.move(key,dbIndex);
		pools.returnResource(sj);
		return value;
	}

	@Override
	public String setkey(String key, String values) throws TException {
		sj = pools.getResource();
		String value = sj.set(key,values);
		pools.returnResource(sj);
		return value;
	}

	@Override
	public boolean setbit(String key, long offset, boolean values)
			throws TException {
		sj = pools.getResource();
		boolean value = sj.setbit(key,offset,values);
		pools.returnResource(sj);
		return value;
	}

	@Override
	public long setnx(String key, String values) throws TException {
		sj = pools.getResource();
		long value = sj.setnx(key,values);
		pools.returnResource(sj);
		return value;
	}

	@Override
	public long incr(String key) throws TException {
		sj = pools.getResource();
		long value = sj.incr(key);
		pools.returnResource(sj);
		return value;
	}

	@Override
	public long hset(String key, String field, String values) throws TException {
		sj = pools.getResource();
		long value = sj.hset(key,field,values);
		pools.returnResource(sj);
		return value;
	}

	@Override
	public long hsetnx(String key, String field, String values)
			throws TException {
		sj = pools.getResource();
		long value = sj.hsetnx(key,field,values);
		pools.returnResource(sj);
		return value;
	}

	@Override
	public String hmset(String key, Map<String, String> hash) throws TException {
		sj = pools.getResource();
		String value = sj.hmset(key,hash);
		pools.returnResource(sj);
		return value;
	}

	@Override
	public long hdel(String key, String field) throws TException {
		sj = pools.getResource();
		long value = sj.hdel(key,field);
		pools.returnResource(sj);
		return value;
	}

	@Override
	public long rpush(String key, String field) throws TException {
		sj = pools.getResource();
		long value = sj.rpush(key,field);
		pools.returnResource(sj);
		return value;
	}

	@Override
	public long lpush(String key, String field) throws TException {
		sj = pools.getResource();
		long value = sj.lpush(key,field);
		pools.returnResource(sj);
		return value;
	}

	@Override
	public String lset(String key, long index, String values) throws TException {
		sj = pools.getResource();
		String value = sj.lset(key,index,values);
		pools.returnResource(sj);
		return value;
	}

	@Override
	public long lrem(String key, long count, String values) throws TException {
		sj = pools.getResource();
		long value = sj.lrem(key,count,values);
		pools.returnResource(sj);
		return value;
	}

	@Override
	public long sadd(String key, String member) throws TException {
		sj = pools.getResource();
		long value = sj.sadd(key,member);
		pools.returnResource(sj);
		return value;
	}

	@Override
	public String srandmemberlist(String key, int count) throws TException {
		sj = pools.getResource();
		List<String> lists = new ArrayList<String>();
		if(count >= 0){
			for(int i=0;i<count;i++){
				lists.add(sj.srandmember(key));
			}
			pools.returnResource(sj);
			return JSON.toJSONString(lists);
		}
		String value = sj.srandmember(key);
		pools.returnResource(sj);
		return value;
	}

	@Override
	public long zadd(String key, double score, String member) throws TException {
		sj = pools.getResource();
		long value = sj.zadd(key, score, member);
		pools.returnResource(sj);
		return value;
	}

	@Override
	public long zadds(String key, Map<String, Double> scoreMembers)
			throws TException {
		sj = pools.getResource();
		Map<Double,String> maps = new HashMap<Double,String>();
		for(String map:scoreMembers.keySet()){
			maps.put(scoreMembers.get(map),map);
		}
		//long value = sj.zadd(key, maps);
		return 0;
	}

	@Override
	public String zrangeWithScores(String key, long start, long ends)
			throws TException {
		sj = pools.getResource();
		String value = JSON.toJSONString(sj.zrange(key, start, ends));
		pools.returnResource(sj);
		return value;
	}

	@Override
	public String zrevrangeWithScores(String key, long start, long ends)
			throws TException {
		sj = pools.getResource();
		String value = JSON.toJSONString(sj.zrevrangeWithScores(key, start, ends));
		pools.returnResource(sj);
		return value;
	}

	@Override
	public String zrangeByScore(String key, String min, String max)
			throws TException {
		sj = pools.getResource();
		String value = JSON.toJSONString(sj.zrangeByScore(key, min, max));
		pools.returnResource(sj);
		return value;
	}

	@Override
	public String zrangeByScoreN(String key, String min, String max,
			int offset, int count) throws TException {
		sj = pools.getResource();
		String value = JSON.toJSONString(sj.zrangeByScore(key, min, max,offset,count));
		pools.returnResource(sj);
		return value;
		
	}

	@Override
	public String zrevrangeByScore(String key, String max, String min)
			throws TException {
		sj = pools.getResource();
		String value = JSON.toJSONString(sj.zrevrangeByScore(key, max, min));
		pools.returnResource(sj);
		return value;
		
	}

	@Override
	public String zrevrangeByScoreN(String key, String max, String min,
			int offset, int count) throws TException {
		sj = pools.getResource();
		String value = JSON.toJSONString(sj.zrevrangeByScore(key, max, min,offset,count));
		pools.returnResource(sj);
		return value;
		
	}

	@Override
	public String zrangeByScoreWithScores(String key, String min, String max)
			throws TException {
		sj = pools.getResource();
		String value = JSON.toJSONString(sj.zrangeByScoreWithScores(key, min, max));
		pools.returnResource(sj);
		return value;
		
	}

	@Override
	public String zrevrangeByScoreWithScores(String key, String max, String min)
			throws TException {
		sj = pools.getResource();
		String value = JSON.toJSONString(sj.zrevrangeByScoreWithScores(key, max, min));
		pools.returnResource(sj);
		return value;
		
	}

	@Override
	public String zrangeByScoreWithScoresN(String key, String min, String max,
			int offset, int count) throws TException {
		sj = pools.getResource();
		String value = JSON.toJSONString(sj.zrangeByScoreWithScores(key, min, max,offset,count));
		pools.returnResource(sj);
		return value;
		
	}

	@Override
	public String zrevrangeByScoreWithScoresN(String key, String max,
			String min, int offset, int count) throws TException {
		sj = pools.getResource();
		String value = JSON.toJSONString(sj.zrevrangeByScoreWithScores(key, max, min,offset,count));
		pools.returnResource(sj);
		return value;
	
	}

	@Override
	public long delkey(String key) throws TException {
		sj = pools.getResource();
		long value = sj.del(key);
		pools.returnResource(sj);
		return value;
	}

	@Override
	public String zrange(String key, long start, long ends) throws TException {
		sj = pools.getResource();
		String value = JSON.toJSONString(sj.zrange(key,start,ends));
		pools.returnResource(sj);
		return value;
		
	}


}
