package com.thrift.redis.commands;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.thrift.TException;

import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.JedisShardInfo;
import redis.clients.jedis.ShardedJedis;
import redis.clients.jedis.ShardedJedisPool;

public class MainTest {
	public static void main(String[] args) throws TException, FileNotFoundException, IOException {
		
		
		JedisShardInfo si1 = new JedisShardInfo("54.186.59.210",6379,15000,1);
		List<JedisShardInfo> shards = new ArrayList<JedisShardInfo>();
		shards.add(si1);
		
		RedisCommandsImpl commands = new RedisCommandsImpl(shards);
		System.out.println(commands.get("port"));
	}

}
