package com.thrift;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class DateUtil {
	
	
	private static SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
	
	public static String format(Date date){
		synchronized(df){
			df.setTimeZone(TimeZone.getTimeZone("GMT+8"));
			return df.format(date);
		}
	}
	
	public static Date parse(String str) throws ParseException{
		synchronized(df){
			df.setTimeZone(TimeZone.getTimeZone("GMT+8"));
			return df.parse(str);
		}
	}

}
