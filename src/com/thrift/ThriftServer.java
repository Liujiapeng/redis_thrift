package com.thrift;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.thrift.TProcessor;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.server.TServer;
import org.apache.thrift.server.TSimpleServer;
import org.apache.thrift.transport.TServerSocket;
import org.apache.thrift.transport.TTransportException;

import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.JedisSentinelPool;
import redis.clients.jedis.JedisShardInfo;
import redis.clients.jedis.ShardedJedis;
import redis.clients.jedis.ShardedJedisPool;

import com.thrift.redis.commands.RedisCommands;
import com.thrift.redis.commands.RedisCommandsImpl;
import com.thrift.redis.mail.MailSenderInfo;
import com.thrift.redis.mail.SimpleMailSender;

public class ThriftServer {
	static ShardedJedisPool pool;
	private static int masterPort = 6379;
	private static String masterIp = "";
	private static int slavePort = 6380;
	private static String slaveIp = "";
	private static Properties prop = null;
	private static JedisShardInfo si1 = null;
	private static JedisShardInfo si2 = null;
	
	private static Set<String> sentinels = null;
	private static String currMaster = "";
	
	private static JedisSentinelPool sentinelPool = null;
	
	private static JedisPoolConfig config = null;
	private static List<JedisShardInfo> shards1 = new ArrayList<JedisShardInfo>();
	private static List<JedisShardInfo> shards2 = new ArrayList<JedisShardInfo>();
	public static void startServer() throws TTransportException, FileNotFoundException, IOException{
		TProcessor tprocessor = new RedisCommands.Processor<RedisCommands.Iface>(new RedisCommandsImpl(getShards()));
		
		TServerSocket servertransport= new TServerSocket(10000);
		TServer.Args tArgs = new TServer.Args(servertransport);
		tArgs.processor(tprocessor);
		tArgs.protocolFactory(new TBinaryProtocol.Factory());
		//tArgs.protocolFactory(new TCompactProtocol.Factory());
		//tArgs.protocolFactory(new TJSONProtocol.Factory());
		
		TServer server = new TSimpleServer(tArgs);
		
		server.serve();
	
	}
	
	public static void main(String[] args) throws TTransportException, FileNotFoundException, IOException {
		ThriftServer.startServer();
	}
	public static List<JedisShardInfo> getShards() {
		
		prop = new Properties();
		InputStream is = null;
		try {
			is = new FileInputStream("config/config.properties");
			prop.load(is);
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			try {
				is.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		masterIp = prop.getProperty("masterIp");
		masterPort = Integer.parseInt(prop.getProperty("masterPort"));
		slaveIp = prop.getProperty("slaveIp");
		slavePort = Integer.parseInt(prop.getProperty("slavePort"));
		si1= new JedisShardInfo(masterIp,masterPort,15000,1);
		shards1.add(si1);
		
		currMaster = masterIp+":"+masterPort;
		System.out.println("Current Master : "+currMaster);
		sentinels = new HashSet<String>();
		sentinels.add(new HostAndPort(prop.getProperty("sentinelIp"),Integer.parseInt(prop.getProperty("sentinelPort"))).toString());
		
		sentinelPool = new JedisSentinelPool("mymaster",sentinels);
		//轮训查看主节点是否正常
		Timer timer = new Timer();
		TimerTask task = new TimerTask(){
			@Override
			public void run() {
				
				String master = sentinelPool.getCurrentHostMaster().toString();
				
				//如果上次的主节点和现在的不同，就认为主节点死了，下面进行主从结点的切换
				if(!master.equals(currMaster)){
					System.out.println("Master : " + master);
					sendMail("Redis服务器主节点故障！！！！","redis主节点："+masterIp+":"+masterPort+"故障,现在为您进行主从结点切换,请尽快修复故障！！！");
					si2 = new JedisShardInfo(slaveIp,slavePort,15000,1);
					shards2.add(si2);					
					RedisCommandsImpl.pools = null;
				    RedisCommandsImpl.pools = new ShardedJedisPool(RedisCommandsImpl.config,shards2);
				    
				    prop = new Properties();
					InputStream is = null;
					try {
						is = new FileInputStream("config/config.properties");
						prop.load(is);
						
						//修改配置文件，交换主从的地址和端口
						String tempIp = prop.getProperty("masterIp");
						String tempPort = prop.getProperty("masterPort");
						prop.setProperty("masterIp", prop.getProperty("slaveIp"));
						prop.setProperty("masterPort", prop.getProperty("masterPort"));
						prop.setProperty("slaveIp", tempIp);
						prop.setProperty("slavePort", tempPort);
					} catch (IOException e) {
						e.printStackTrace();
					}finally{
						try {
							is.close();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
					
					
					currMaster = master;
					
				}
				
				
			}
			
		};
		
		timer.schedule(task, 1000, 3000);
		
		//System.out.println(shards1.isEmpty());
		return shards1;
	}
	
	public static void sendMail(String subject,String content){
		
		
		//这个类主要是设置邮件   
	    MailSenderInfo mailInfo = new MailSenderInfo();    
	    mailInfo.setMailServerHost("smtp.ym.163.com");    
	    mailInfo.setMailServerPort("25");    
	    mailInfo.setValidate(true);    
	    mailInfo.setUserName("jiapeng.Liu@xinmei365.com");    
	    mailInfo.setPassword("851114123");//您的邮箱密码    
	    mailInfo.setFromAddress("jiapeng.Liu@xinmei365.com");    
	    mailInfo.setToAddress("jiapeng.Liu@xinmei365.com");  
	    mailInfo.setToAddress(prop.getProperty("email1"));
	    mailInfo.setToAddress(prop.getProperty("email2"));
	    mailInfo.setSubject(subject);    
	    mailInfo.setContent(content);    
	       //这个类主要来发送邮件   
	    SimpleMailSender sms = new SimpleMailSender();   
	       sms.sendTextMail(mailInfo);//发送文体格式    
	      //  sms.sendHtmlMail(mailInfo);//发送html格式   
	}
	
	
	
	

}
