package com.thrift;

import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;



import com.thrift.redis.commands.RedisCommands;
//import com.thrift.redis.commands.RedisCommands;
import com.thrift.redis.commands.RedisCommands1;

public class ThriftClient {
	
	public static void startClient(){
		TTransport transport = null;
		try{
			transport = new TSocket("54.186.59.210",10000,30000);
			//协议要和服务端一致
			TProtocol protocol = new TBinaryProtocol(transport);
			//TProtocol protocol = new TCompactProtocol(transport);
			//TProtocol protocol = new TJSONProtocol(transport);
			//RedisCommands是完整版，RedisCommands1是缩略版只能读
			RedisCommands.Client client = new RedisCommands.Client(protocol);
			transport.open();
			System.out.println(client.get("port"));
			
			//System.out.println(client.zcount("database", "1","18"));
			//System.out.println(client.zcard("language"));
			//System.out.println(client.zrangeByScore("database", "1", "100"));
			
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(null!=transport){
				transport.close();
			}
		}
	}
	
	public static void main(String[] args) {
		ThriftClient.startClient();	
		
	}

}
